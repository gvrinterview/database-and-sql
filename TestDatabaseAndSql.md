# Questions

1. What does the acronym "SQL" mean ?

- Sequence Query Languages
- Structured Query Language
- Settings Query Literal
- Set Query Languages

2. Which SQL statement is used to extract data from a database?

- OPEN
- SELECT
- EXTRACT
- GETTER

3. Which SQL statement is used to return only different values?

- SELECT SETTINGS
- SELECT DIFFERENT
- SELECT DISTINCT
- SELECT ONLY

4. With SQL, how do you select all the records from a table named "Persons" where the "LastName" is alphabetically between (and including) "Hansen" and "Pettersen"?

- SELECT * FROM Persons WHERE LastName BETWEEN 'Hansen' AND 'Pettersen'  
- SELECT LastName>'Hansen' AND LastName<'Pettersen' FROM Persons
- SELECT * FROM Persons WHERE LastName>'Hansen' AND LastName<'Pettersen'
- SELECT TOP 10 FROM Persons WHERE Lastname is equal 'Hansen' OR LastName incluse 'Pettersen'

5. Using this [site](https://www.programiz.com/sql/online-compiler/) , create the following queries in SQL language :
- For customer John Reinhardt age 25 and country UK , extrapolate all orders placed and the status of those orders
- Extrapolate all customers sorted by ascending age who placed 'Keyboard' orders with amount greater than or equal to 200
- Extrapolate order number, item, amount, customer's first and last name where order status is 'Delivered'.

6. Explain the difference between Database and DBMS

7. Explain the difference between primary key and foreign key

8. What is meant by "data redundancy" ?

9. What is the concept of subqueries in terms of SQL?

10. What is the use of the DROP command and what are the differences between the DROP, TRUNCATE and DELETE commands?
